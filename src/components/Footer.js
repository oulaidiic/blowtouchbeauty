import React from "react";

const Footer = (props) => (
    <footer className="page_footer page_footer2 overflow-visible  ls s-overlay s-parallax s-pt-50 s-pb-50 c-mb-30 s-py-md-100 s-py-lg-150 s-py-xl-0 c-gutter-60 my-50">
        <div className="container overflow-visible">
            <div className="row">
                <div className="col-lg-6 col-xl-4 d-flex flex-column align-items-center  animate" data-animation="fadeInUp">
                    <div className="fw-divider-space hidden-below-xl mt-150"></div>
                    <h3 className="text-center">Contacts</h3>
                    <div className="media">
                        <div className="icon-styled color-main fs-14">
                            <i className="flaticon-headphones"></i>
                        </div>
                        <p className="media-body">(800) 123-45-67, (800) 123-45-68</p>
                    </div>
                    <div className="media">
                        <div className="icon-styled color-main fs-14">
                            <i className="flaticon-envelope"></i>
                        </div>
                        <p className="media-body">
                            <a href="#">info@blowtouch.com</a>
                        </p>
                    </div>
                    <div className="media pb-10 pb-lg-30">
                        <div className="icon-styled color-main fs-14">
                            <i className="flaticon-placeholder"></i>
                        </div>
                        <p className="media-body">3206 Hickman St., San Francisco, CA</p>
                    </div>
                    <p className="social-icons text-center">
                        <a href="#" className="fa fa-facebook border-icon rounded-icon" title="facebook"></a>
                        <a href="#" className="fa fa-twitter border-icon rounded-icon" title="twitter"></a>
                        <a href="#" className="fa fa-google-plus border-icon rounded-icon" title="google"></a>
                    </p>
                </div>
                <div className="d-none d-xl-flex col-xl-4 animate text-center flex-column align-items-center justify-content-center" data-animation="fadeInUp">
                    <div className="wrap s-px-lg-10 ds ms">
                        <div className="fw-divider-space hidden-below-md mt-160"></div>
                        <div className="fw-divider-space hidden-above-md mt-20"></div>
                        {/* <img src="images/logo-black.png" alt="img" /> */}
                        <p>
                            REDISCOVER YOURSELF.
								</p>
                        <div>
                        <div className="page_copyright text-center mt-auto">
                            <h6>We accept crypto</h6>
                            </div>
                            <i className="fs-27 fa fa-cc-visa"></i>
                            <i className="fs-27 fa fa-cc-discover"></i>
                            <i className="fs-27 fa fa-cc-mastercard"></i>
                            <i className="fs-27 fa fa-btc"></i>
                            <i className="fs-27 fa fa-cc-paypal"></i>
                        </div>
                        <div className="fw-divider-space hidden-below-md mt-140"></div>
                        <div className="fw-divider-space hidden-above-md mt-20"></div>
                        <div className="page_copyright text-center mt-auto">
                            <p>©  Blowtouch Copyright 2021</p>
                        </div>
                    </div>
                </div>
                <div className="col-lg-6 col-xl-4 text-center d-flex flex-column align-items-center  animate" data-animation="fadeInUp">
                    <div className="fw-divider-space hidden-below-xl mt-150"></div>
                    <h4 className="">Connect with us</h4>
                    <div className="widget widget_mailchimp pencil mb-0">
                        <p>
                            Please register your e-mail to receive our 10 % off your first booking.
								</p>

                        <form className="signup" action="/">
                            <label htmlFor="mailchimp_email">
                                <span className="screen-reader-text">Subscribe:</span>
                            </label>
                            <i className="flaticon-envelope"></i>
                            <input id="mailchimp_email" name="email" type="email" className="form-control mailchimp_email" placeholder="Email address" />

                            <button type="submit" className="search-submit">
                                <i className="fa fa-pencil"></i>
                            </button>
                            <div className="response"></div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </footer>
);
export default Footer;