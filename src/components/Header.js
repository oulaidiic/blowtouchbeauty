import React , { useState , useEffect } from 'react';

import logo from '../assets/images/logo.png';
import shop26 from '../assets/images/shop/26.jpg';

import Login from '../pages/Homepage/Login';

import Register from '../pages/Homepage/Register';


export function Header(props){

    const [formType , setFormType] = useState('login');



return(
    
    <header className="page_header ls">
        <div className="container-fluid">
            <div className="row align-items-center">
                <div className="col-8 col-xl-2 text-left">
                    <a href="./" className="logo">
                        {/* <img src={logo} alt="" /> */}
                    </a>
                </div>
                <div className="col-xl-8 col-1 text-center">
                    <nav className="top-nav">
                        <ul className="nav sf-menu">
                            <li className="active">
                                <a href="index.html">Home</a>
                                <ul>
                                    <li>
                                        <a href="index.html">MultiPage</a>
                                    </li>
                                    <li>
                                        <a href="index_static.html">Static Intro</a>
                                    </li>
                                    <li>
                                        <a href="index_singlepage.html">Single Page</a>
                                    </li>
                                    <li>
                                        <a href="index-alt.html">Single Page Alternative</a>
                                    </li>
                                </ul>
                            </li>
                            {/* <!-- blog --> */}
                            <li>
                                <a href="blog-right.html">Blog</a>
                                <ul>

                                    <li>
                                        <a href="blog-right.html">Right Sidebar</a>
                                    </li>
                                    <li>
                                        <a href="blog-left.html">Left Sidebar</a>
                                    </li>
                                    <li>
                                        <a href="blog-full.html">No Sidebar</a>
                                    </li>
                                    <li>
                                        <a href="blog-grid.html">Blog Grid</a>
                                    </li>

                                    <li>
                                        <a href="blog-single-right.html">Post</a>
                                        <ul>
                                            <li>
                                                <a href="blog-single-right.html">Right Sidebar</a>
                                            </li>
                                            <li>
                                                <a href="blog-single-left.html">Left Sidebar</a>
                                            </li>
                                            <li>
                                                <a href="blog-single-full.html">No Sidebar</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="blog-single-video-right.html">Video Post</a>
                                        <ul>
                                            <li>
                                                <a href="blog-single-video-right.html">Right Sidebar</a>
                                            </li>
                                            <li>
                                                <a href="blog-single-video-left.html">Left Sidebar</a>
                                            </li>
                                            <li>
                                                <a href="blog-single-video-full.html">No Sidebar</a>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>
                            {/* <!-- eof blog --> */}

                            <li>
                                <a href="#">Features</a>
                                <div className="mega-menu">
                                    <ul className="mega-menu-row">
                                        <li className="mega-menu-col">
                                            <a href="#">Headers</a>
                                            <ul>
                                                <li>
                                                    <a href="header1.html">Header Type 1</a>
                                                </li>
                                                <li>
                                                    <a href="header2.html">Header Type 2</a>
                                                </li>
                                                <li>
                                                    <a href="header3.html">Header Type 3</a>
                                                </li>
                                                <li>
                                                    <a href="header4.html">Header Type 4</a>
                                                </li>
                                                <li>
                                                    <a href="header5.html">Header Type 5</a>
                                                </li>
                                                <li>
                                                    <a href="header6.html">Header Type 6</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="mega-menu-col">
                                            <a href="#">Side Menus</a>
                                            <ul>
                                                <li>
                                                    <a href="header-side.html">Push Left</a>
                                                </li>
                                                <li>
                                                    <a href="header-side-right.html">Push Right</a>
                                                </li>
                                                <li>
                                                    <a href="header-side-slide.html">Slide Left</a>
                                                </li>
                                                <li>
                                                    <a href="header-side-slide-right.html">Slide Right</a>
                                                </li>
                                                <li>
                                                    <a href="header-side-sticked.html">Sticked Left</a>
                                                </li>
                                                <li>
                                                    <a href="header-side-sticked-right.html">Sticked Right</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="mega-menu-col">
                                            <a href="title1.html">Title Sections</a>
                                            <ul>
                                                <li>
                                                    <a href="title1.html">Title section 1</a>
                                                </li>
                                                <li>
                                                    <a href="title2.html">Title section 2</a>
                                                </li>
                                                <li>
                                                    <a href="title3.html">Title section 3</a>
                                                </li>
                                                <li>
                                                    <a href="title4.html">Title section 4</a>
                                                </li>
                                                <li>
                                                    <a href="title5.html">Title section 5</a>
                                                </li>
                                                <li>
                                                    <a href="title6.html">Title section 6</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="mega-menu-col">
                                            <a href="footer1.html#footer">Footers</a>
                                            <ul>
                                                <li>
                                                    <a href="footer1.html#footer">Footer Type 1</a>
                                                </li>
                                                <li>
                                                    <a href="footer2.html#footer">Footer Type 2</a>
                                                </li>
                                                <li>
                                                    <a href="footer3.html#footer">Footer Type 3</a>
                                                </li>
                                                <li>
                                                    <a href="footer4.html#footer">Footer Type 4</a>
                                                </li>
                                                <li>
                                                    <a href="footer5.html#footer">Footer Type 5</a>
                                                </li>
                                                <li>
                                                    <a href="footer6.html#footer">Footer Type 6</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="mega-menu-col">
                                            <a href="copyright1.html#copyright">Copyright</a>

                                            <ul>
                                                <li>
                                                    <a href="copyright1.html#copyright">Copyright 1</a>
                                                </li>
                                                <li>
                                                    <a href="copyright2.html#copyright">Copyright 2</a>
                                                </li>
                                                <li>
                                                    <a href="copyright3.html#copyright">Copyright 3</a>
                                                </li>
                                                <li>
                                                    <a href="copyright4.html#copyright">Copyright 4</a>
                                                </li>
                                                <li>
                                                    <a href="copyright5.html#copyright">Copyright 5</a>
                                                </li>
                                                <li>
                                                    <a href="copyright6.html#copyright">Copyright 6</a>
                                                </li>
                                            </ul>
                                        </li>

                                    </ul>
                                </div>
                                {/* <!-- eof mega menu --> */}
                            </li>
                            {/* <!-- eof features --> */}


                            <li>
                                <a href="about.html">Pages</a>
                                <ul>


                                    <li>
                                        <a href="about.html">About</a>
                                    </li>

                                    <li>
                                        <a href="services.html">Our Services</a>
                                        <ul>
                                            <li>
                                                <a href="services.html">Services 1</a>
                                            </li>
                                            <li>
                                                <a href="services2.html">Services 2</a>
                                            </li>
                                            <li>
                                                <a href="services3.html">Services 3</a>
                                            </li>
                                            <li>
                                                <a href="service-single.html">Single Service</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="shop-right.html">Shop</a>
                                        <ul>
                                            <li>
                                                <a href="shop-account-dashboard.html">Account</a>
                                                <ul>

                                                    <li>
                                                        <a href="shop-account-details.html">Account details</a>
                                                    </li>
                                                    <li>
                                                        <a href="shop-account-addresses.html">Addresses</a>
                                                    </li>
                                                    <li>
                                                        <a href="shop-account-address-edit.html">Edit Address</a>
                                                    </li>
                                                    <li>
                                                        <a href="shop-account-orders.html">Orders</a>
                                                    </li>
                                                    <li>
                                                        <a href="shop-account-order-single.html">Single Order</a>
                                                    </li>
                                                    <li>
                                                        <a href="shop-account-downloads.html">Downloads</a>
                                                    </li>
                                                    <li>
                                                        <a href="shop-account-password-reset.html">Password Reset</a>
                                                    </li>
                                                    <li>
                                                        <a href="shop-account-login.html">Login/Logout</a>
                                                    </li>

                                                </ul>
                                            </li>
                                            <li>
                                                <a href="shop-right.html">Right Sidebar</a>
                                            </li>
                                            <li>
                                                <a href="shop-left.html">Left Sidebar</a>
                                            </li>
                                            <li>
                                                <a href="shop-product-right.html">Product Right Sidebar</a>
                                            </li>
                                            <li>
                                                <a href="shop-product-left.html">Product Left Sidebar</a>
                                            </li>
                                            <li>
                                                <a href="shop-cart.html">Cart</a>
                                            </li>
                                            <li>
                                                <a href="shop-checkout.html">Checkout</a>
                                            </li>
                                            <li>
                                                <a href="shop-order-received.html">Order Received</a>
                                            </li>

                                        </ul>
                                    </li>
                                    {/* <!-- eof shop --> */}

                                    {/* <!-- features --> */}
                                    <li>
                                        <a href="shortcodes_iconbox.html">Shortcodes</a>
                                        <ul>
                                            <li>
                                                <a href="shortcodes_typography.html">Typography</a>
                                            </li>
                                            <li>
                                                <a href="shortcodes_buttons.html">Buttons</a>
                                            </li>
                                            <li>
                                                <a href="shortcodes_pricing.html">Pricing</a>
                                            </li>
                                            <li>
                                                <a href="shortcodes_iconbox.html">Icon Boxes</a>
                                            </li>
                                            <li>
                                                <a href="shortcodes_progress.html">Progress</a>
                                            </li>
                                            <li>
                                                <a href="shortcodes_tabs.html">Tabs &amp; Collapse</a>
                                            </li>
                                            <li>
                                                <a href="shortcodes_bootstrap.html">Bootstrap Elements</a>
                                            </li>
                                            <li>
                                                <a href="shortcodes_animation.html">Animation</a>
                                            </li>
                                            <li>
                                                <a href="shortcodes_icons.html">Template Icons</a>
                                            </li>
                                            <li>
                                                <a href="shortcodes_socialicons.html">Social Icons</a>
                                            </li>
                                        </ul>
                                    </li>
                                    {/* <!-- eof shortcodes --> */}

                                    <li>
                                        <a href="shortcodes_widgets_default.html">Widgets</a>
                                        <ul>
                                            <li>
                                                <a href="shortcodes_widgets_default.html">Default Widgets</a>
                                            </li>
                                            <li>
                                                <a href="shortcodes_widgets_shop.html">Shop Widgets</a>
                                            </li>
                                            <li>
                                                <a href="shortcodes_widgets_custom.html">Custom Widgets</a>
                                            </li>
                                        </ul>

                                    </li>


                                    {/* <!-- events --> */}
                                    <li>
                                        <a href="events-left.html">Events</a>
                                        <ul>
                                            <li>
                                                <a href="events-left.html">Left Sidebar</a>
                                            </li>
                                            <li>
                                                <a href="events-right.html">Right Sidebar</a>
                                            </li>
                                            <li>
                                                <a href="events-full.html">Full Width</a>
                                            </li>
                                            <li>
                                                <a href="event-single-left.html">Single Event</a>
                                                <ul>
                                                    <li>
                                                        <a href="event-single-left.html">Left Sidebar</a>
                                                    </li>
                                                    <li>
                                                        <a href="event-single-right.html">Right Sidebar</a>
                                                    </li>
                                                    <li>
                                                        <a href="event-single-full.html">Full Width</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    {/* <!-- eof events --> */}

                                    <li>
                                        <a href="team.html">Team</a>
                                        <ul>
                                            <li>
                                                <a href="team.html">Team List</a>
                                            </li>
                                            <li>
                                                <a href="team-single.html">Team Member</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="pricing.html">Pricing</a>
                                    </li>

                                    <li>
                                        <a href="comingsoon.html">Comingsoon</a>
                                    </li>

                                    <li>
                                        <a href="faq.html">FAQ</a>
                                        <ul>
                                            <li>
                                                <a href="faq.html">FAQ</a>
                                            </li>
                                            <li>
                                                <a href="faq2.html">FAQ 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="404.html">404</a>
                                    </li>

                                </ul>
                            </li>
                            <li>
                                <a href="gallery-image.html">Gallery</a>
                                <ul>
                                    {/* <!-- Gallery image only --> */}
                                    <li>
                                        <a href="gallery-image.html">Image Only</a>
                                        <ul>
                                            <li>
                                                <a href="gallery-image-2-cols.html">2 columns</a>
                                            </li>
                                            <li>
                                                <a href="gallery-image.html">3 columns</a>
                                            </li>
                                            <li>
                                                <a href="gallery-image-4-cols-fullwidth.html">4 columns fullwidth</a>
                                            </li>

                                        </ul>
                                    </li>
                                    {/* <!-- eof Gallery image only --> */}

                                    {/* <!-- Gallery with title --> */}
                                    <li>
                                        <a href="gallery-title.html">Image With Title</a>
                                        <ul>
                                            <li>
                                                <a href="gallery-title-2-cols.html">2 columns</a>
                                            </li>
                                            <li>
                                                <a href="gallery-title.html">3 column</a>
                                            </li>
                                            <li>
                                                <a href="gallery-title-4-cols-fullwidth.html">4 columns fullwidth</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="gallery-excerpt.html">Image with Excerpt</a>
                                        <ul>
                                            <li>
                                                <a href="gallery-excerpt-2-cols.html">2 columns</a>
                                            </li>
                                            <li>
                                                <a href="gallery-excerpt.html">3 column</a>
                                            </li>
                                            <li>
                                                <a href="gallery-excerpt-4-cols-fullwidth.html">4 columns fullwdith</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="gallery-tiled.html">Tiled Gallery</a>
                                    </li>

                                    <li>
                                        <a href="gallery-single.html">Gallery Item</a>
                                        <ul>
                                            <li>
                                                <a href="gallery-single.html">Style 1</a>
                                            </li>
                                            <li>
                                                <a href="gallery-single2.html">Style 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="contact.html">Contact</a>
                                <ul>
                                    <li>
                                        <a href="contact.html">Contact 1</a>
                                    </li>
                                    <li>
                                        <a href="contact2.html">Contact 2</a>
                                    </li>
                                    <li>
                                        <a href="contact3.html">Contact 3</a>
                                    </li>
                                    <li>
                                        <a href="contact4.html">Contact 4</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>

                </div>
                <div className="col-1 col-xl-2 ">
                    <div className="header-includ">
                        {/* <!--user action--> */}
                        <div className="dropdown">
                            <a className="dropdown-toggle dropdown-shopping-cart" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i className="flaticon-bag"></i>
                                <span className="badge bg-maincolor">2</span>
                            </a>
                            <div className="dropdown-menu dropdown-menu-right ls">
                                <div className="widget woocommerce widget_shopping_cart">


                                    <div className="widget_shopping_cart_content">

                                        <ul className="woocommerce-mini-cart cart_list product_list_widget ">
                                            <li className="woocommerce-mini-cart-item mini_cart_item">
                                                <a href="#" className="remove" aria-label="Remove this item" data-product_id="73" data-product_sku="">×</a>
                                                <a href="shop-product-right.html">
                                                    <img src={shop26} alt="" />Ship Your Idea
                                                </a>

                                                <span className="quantity">1 ×
                                                    <span className="woocommerce-Price-amount amount">
                                                        <span className="woocommerce-Price-currencySymbol">$</span>
                                                        12.00
                                                    </span>
                                                </span>
                                            </li>
                                            <li className="woocommerce-mini-cart-item mini_cart_item">
                                                <a href="#" className="remove" aria-label="Remove this item" data-product_id="76" data-product_sku="">×</a>
                                                <a href="shop-product-right.html">
                                                    <img src={shop26} alt="" />Woo Album #1
                                                </a>

                                                <span className="quantity">1 ×
                                                    <span className="woocommerce-Price-amount amount">
                                                        <span className="woocommerce-Price-currencySymbol">$</span>
                                                        15.00
                                                    </span>
                                                </span>
                                            </li>
                                        </ul>

                                        <p className="woocommerce-mini-cart__total total">
                                            <strong>Subtotal:</strong>
                                            <span className="woocommerce-Price-amount amount">
                                                <span className="woocommerce-Price-currencySymbol">$</span>
                                                27.00
                                            </span>
                                        </p>

                                        <p className="woocommerce-mini-cart__buttons buttons">
                                            <a href="shop-cart.html" className="button wc-forward">View cart</a>
                                            <a href="shop-cart.html" className="button wc-forward checkout">checkout</a>
                                        </p>

                                    </div>
                                </div>

                            </div>
                        </div>


                        {/* <!--login form --> */}
                        <div className="dropdown">
                            <a className="" href="#" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
                                <i className="flaticon-profile"></i>
                            </a>

                            <div className="dropdown-menu dropdown-menu-right text-center">

                                <div className="dropdown-top">
                                    <a onClick={(e) => {e.stopPropagation() ; setFormType('login')}}>log in</a>
                                    <a onClick={(e) => {e.stopPropagation() ; setFormType('register')}}>register</a>
                                </div>

                                {
                                    formType === 'login' && <Login />


                                }
                                 {
                                    formType === 'register' && <Register/>
                                }
                            </div>

                        </div>

                        <span>
                            <a href="#" className="search_modal_button">
                                <i className="flaticon-search"></i>
                            </a>
                        </span>

                    </div>

                </div>
            </div>
        </div>

        <span className="toggle_menu"><span></span></span>
    </header>
);
}
export default Header;
