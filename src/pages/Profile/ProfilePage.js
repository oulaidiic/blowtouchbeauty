import React from "react";
import Footer from "../../components/Footer";
import PreloaderImg from "../Homepage/PreloaderImg";
import Modal from "../Homepage/Modal";
import TopLine from "../Homepage/TopLine";
import Header from "../../components/Header";
import PageSlider from "../Homepage/PageSlider";
import Gallery from "../Homepage/Gallery";
import Welcome from "../Homepage/Welcome";
import Video from "../Homepage/Video";
import Appointment from "../Homepage/Appointment";
import Service from "../Homepage/Service";
import Portfolio from "../Homepage/Portfolio";
import Quote from "../Homepage/Quote";
import Post from "../Homepage/Post";
import Team from "../Homepage/Team";
import ProfileMain from "./ProfileMain";
import ImagesSlider from "../Homepage/ImagesSlider";

const ProfilePage = (props) => (
    <>
        <PreloaderImg />
        <Modal />
        <div id="canvas">
            <div id="box_wrapper">
                <TopLine />
                <Header />
                <ProfileMain />
                {/* <Gallery /> */}
                {/* <Welcome /> */}
                {/* <Video /> */}
                {/* <Appointment /> */}
                {/* <Service />
                <Portfolio />
                <Quote />
                <Post /> */}
                {/* <Team />
                <ImagesSlider /> */}
            </div>
        </div>
        <Footer />
    </>
);
export default ProfilePage;