import React from "react";
import ProfileIntro from "./ProfileIntro";
import team1 from '../../assets/images/team/01.jpg';
const ProfileMain = (props) => (

<div>
  
  <section className="ls team team-single s-pt-60 s-pb-30 s-pt-lg-100 s-pb-lg-40 s-pt-xl-160 s-pb-xl-160 c-mb-30 c-mb-lg-30 c-mb-xl-0">
    <div className="container">
      <div className="row">
        <div className="col-12">
          <div className="row c-gutter-60">
           < ProfileIntro />
            <div className="col-md-12 col-lg-8">
              <p>
                Donec rutrum vestibulum risus, non faucibus turpis gravida at. Aliquam ut egestas ante. Etiam vulputate at magna sit amet sollicitudin. Nunc dignissim eros ut neque faucibus.
              </p>
              <p>
                Non dictum neque euismod. Fusce ultricies blandit orci, vel interdum enim condimentum et. Fusce vitae bibendum purus. Cras pellentesque lorem eget.
                Swine brisket burgdoggen pig prosciutto porchetta shoulder jowl ball tip tail jerky. Leberkas cupim.
              </p>
              <ul className="list1">
                <li>
                  <a href="services.html">Suspendisse quis eros tellu sfermentum.</a>
                </li>
                <li>
                  <a href="services.html">Consetetur sadipscing elitr sed diam nonumy.</a>
                </li>
                <li>
                  <a href="services.html">Dolore magna aliquyam erat, sed diam.</a>
                </li>
              </ul>
              <p>
                Drumstick tri-tip cow meatloaf chicken tongue bresaola capicola short loin. Ham hock rump drumstick shankle tenderloin andouille boudin. Tongue cupim swine bacon filet mignon bresaola.
              </p>
              {/* tabs start */}
              <ul className="nav nav-tabs mt-40" role="tablist">
                <li className="nav-item">
                  <a className="nav-link active" id="tab01" data-toggle="tab" href="#tab01_pane" role="tab" aria-controls="tab01_pane" aria-expanded="true">Biography</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" id="tab02" data-toggle="tab" href="#tab02_pane" role="tab" aria-controls="tab02_pane">Skills</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" id="tab03" data-toggle="tab" href="#tab03_pane" role="tab" aria-controls="tab03_pane">Message</a>
                </li>
              </ul>
              <div className="tab-content mb-40">
                <div className="tab-pane fade show active" id="tab01_pane" role="tabpanel" aria-labelledby="tab01">
                  <h5>Biography:</h5>
                  <p>
                    Meatloaf flank pork alcatra. Swine brisket burgdoggen pig prosciutto porchetta shoulder jowl ball tip tail jerky.
                    Leberkas cupim meatloaf flank kevin jowl picanha, capicola brisket jerky venison corned beef meatball.
                  </p>
                  <h5>Professional Life:</h5>
                  <p>
                    Boudin filet mignon alcatra drumstick landjaeger doner chuck ribeye pork chop jerky.
                    Pork belly strip steak short loin salami. Brisket kevin beef porchetta turducke.
                  </p>
                </div>
                <div className="tab-pane fade" id="tab02_pane" role="tabpanel" aria-labelledby="tab02">
                  <p>
                    Dutem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat,
                    vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio
                    dignissim qui blandit. Praesent luptatum zzril delenit augue duis dolore te feugait
                    nulla facilisi.
                  </p>
                  <p className="progress-title">Consulting</p>
                  <div className="progress">
                    <div className="progress-bar bg-maincolor" role="progressbar" data-transitiongoal={90} aria-valuemin={0} aria-valuemax={100}>
                      <span>90%</span>
                    </div>
                  </div>
                  <span className="progress-title">Finance</span>
                  <div className="progress">
                    <div className="progress-bar bg-maincolor2" role="progressbar" data-transitiongoal={98} aria-valuemin={0} aria-valuemax={100}>
                      <span>98%</span>
                    </div>
                  </div>
                  <span className="progress-title">Marketing</span>
                  <div className="progress">
                    <div className="progress-bar bg-maincolor3" role="progressbar" data-transitiongoal={93} aria-valuemin={0} aria-valuemax={100}>
                      <span>93%</span>
                    </div>
                  </div>
                  <p><br />
                    Ut wisi enim ad minim veniaquis nostrud exetation ullamcorper suscipit lobortis nisl
                    ut aliquip ex ea commodo consequat. Dutem vel eum iriure dolor in hendrerit in
                    vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis
                    at vero eros et accumsan et iusto odio dignissim qui blandit.
                  </p>
                </div>
                <div className="tab-pane fade" id="tab03_pane" role="tabpanel" aria-labelledby="tab03">
                  <form className="contact-form form-inverse" method="post" action="/">
                    <p className="contact-form-name form-group">
                      <i className="flaticon-profile" />
                      <input type="text" aria-required="true" size={30} defaultValue name="name" className="form-control" placeholder="Name" />
                    </p>
                    <p className="contact-form-email form-group">
                      <i className="flaticon-envelope" />
                      <input type="email" aria-required="true" size={30} defaultValue name="email" className="form-control" placeholder="Email" />
                    </p>
                    <p className="contact-form-message form-group">
                      <i className="flaticon-clip" />
                      <textarea aria-required="true" rows={8} cols={45} name="message" className="form-control" placeholder="Message" defaultValue={""} />
                    </p>
                    <p className="contact-form-submit form-group">
                      <button type="submit" name="contact_submit" className="btn btn-outline-maincolor">Send Message
                      </button>
                    </p>
                  </form>
                </div>
              </div>
              {/* tabs end*/}
              <p>
                Drumstick tri-tip chicken alcatra tail. Ham hock beef pig salami bacon cupim. Hamburger brisket cupim, sirloin swine pork belly ham doner fatback biltong kielbasa boudin.
              </p>
              <blockquote>
                <footer>
                  <cite>Jesse D. Caballero, <span>Makeup Client</span></cite>
                </footer>
                <p>
                  Lorem ipsum dolor amet, consetetur sadipscing elitr sdiam nonumy eirmod tempor invidunt labore dolore magna aliquyam erat sed diam.
                </p>
              </blockquote>
              <p>
                Porchetta doner frankfurter ground round filet mignon ham hock pork belly ham alcatra burgdoggen tri-tip bacon tail jerky fatback. Porchetta tongue ham hock spare ribs chuck tri-tip.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


);

export default ProfileMain;