import React, { useState } from "react"

import team1 from '../../assets/images/team/01.jpg';

import { useAuth } from "../../contexts/AuthContext"
import { Link, useHistory } from "react-router-dom"

export default function ProfileIntro()  {

    const [error, setError] = useState("")
    const { currentUser, logout } = useAuth()
    const history = useHistory()
  
    async function handleLogout() {
      setError("")
  
      try {
        await logout()
        history.push("/")
      } catch {
        setError("Failed to log out")
      }
    }
return (


<div className="col-md-12 col-lg-4">
<div className="vertical-item content-padding text-center">
  <div className="item-media">
  <img src={team1} alt="img" />
  </div>
  <div className="item-content">
    <h4 className="tile">
    {currentUser.email}
    </h4>
    <span className="color-main">
      Makeup
    </span>
    <p>At vero eos et accusam et justo duo dolores etea.</p>
    <div className="social-icons">
      <a href="#" className="fa fa-facebook" title="facebook" />
      <a href="#" className="fa fa-twitter" title="twitter" />
      <a href="#" className="fa fa-google-plus" title="google" />
    </div>
  </div>
</div>
</div>


)};

