import React from "react";
import team1 from '../../assets/images/team/01.jpg';
import team2 from '../../assets/images/team/02.jpg';
import team3 from '../../assets/images/team/03.jpg';
import team4 from '../../assets/images/team/04.jpg';

const Team = (props) => (
  
    <section className="section-team team ls s-pb-60 s-pb-md-90 s-pb-lg-130 s-pb-xl-220">
          <p>
        
        </p>
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <span className="sub-title text-center">meet our</span>
                    <h3 className="special-heading text-center">creative team</h3>
                    <div className="divider-70 d-none d-xl-block"></div>
                    <div className="divider-30 d-block d-xl-none"></div>
                    <div className="owl-carousel" data-loop="true" data-margin="30" data-nav="true" data-dots="true" data-center="false" data-items="1" data-autoplay="false" data-responsive-xs="1" data-responsive-sm="1" data-responsive-md="2" data-responsive-lg="3">
                        <div className="vertical-item content-padding text-center">
                            <div className="item-media">
                                <img src={team1} alt="img" />
                                <div className="media-links">
                                    <a className="abs-link" title="" href="team-single.html"></a>
                                </div>
                            </div>
                            <div className="item-content">
                                <h4 className="tile">
                                    <a href="team-single.html">Bridgette Jameson</a>
                                </h4>

                                <a href="team-single.html"><span className="color-main">
                                    Hair Stylist
                                        </span></a>
                                <p>FRENCH HAIR CUTTING TECHNIQUES, BALAYAGE, MERMAID HAIR COLOR CORRECTION</p>
                                <div className="social-icons">
                                    <a href="#" className="fa fa-facebook" title="facebook"></a>
                                    <a href="#" className="fa fa-twitter" title="twitter"></a>
                                    <a href="#" className="fa fa-google-plus" title="google"></a>
                                </div>

                            </div>
                        </div>
                        <div className="vertical-item content-padding text-center">
                            <div className="item-media">
                                <img src={team2} alt="img" />
                                <div className="media-links">
                                    <a className="abs-link" title="" href="team-single.html"></a>
                                </div>
                            </div>
                            <div className="item-content">
                                <h4 className="tile">
                                    <a href="team-single.html">Selina Scott</a>
                                </h4>

                                <a href="team-single.html"><span className="color-main">
                                    Makeup
                                        </span></a>
                                <p>EVENT CUSTOM MAKE UP USING LUXURIOUS BRAND TOM FORD , DIOR , MAC ... ETC.</p>
                                <div className="social-icons">
                                    <a href="#" className="fa fa-facebook" title="facebook"></a>
                                    <a href="#" className="fa fa-twitter" title="twitter"></a>
                                    <a href="#" className="fa fa-google-plus" title="google"></a>
                                </div>

                            </div>
                        </div>
                        <div className="vertical-item content-padding text-center">
                            <div className="item-media">
                                <img src={team3} alt="img" />
                                <div className="media-links">
                                    <a className="abs-link" title="" href="team-single.html"></a>
                                </div>
                            </div>
                            <div className="item-content">
                                <h4 className="tile">
                                    <a href="team-single.html">Carlos Linton</a>
                                </h4>

                                <a href="team-single.html"><span className="color-main">
                                    Barber
                                        </span></a>
                                <p>PRECISION & TEXTURED CUTS, WOMEN’S SHORT AND MEN’S CUTS.</p>
                                <div className="social-icons">
                                    <a href="#" className="fa fa-facebook" title="facebook"></a>
                                    <a href="#" className="fa fa-twitter" title="twitter"></a>
                                    <a href="#" className="fa fa-google-plus" title="google"></a>
                                </div>

                            </div>
                        </div>
                        <div className="vertical-item content-padding text-center">
                            <div className="item-media">
                                <img src={team4} alt="img" />
                                <div className="media-links">
                                    <a className="abs-link" title="" href="team-single.html"></a>
                                </div>
                            </div>
                            <div className="item-content">
                                <h4 className="tile">
                                    <a href="team-single.html">Robin Augustine</a>
                                </h4>

                                <a href="team-single.html"><span className="color-main">
                                    Nail Artist
                                        </span></a>
                                <p>Consetetur sadipscing elitr, sed diam nonumy.</p>
                                <div className="social-icons">
                                    <a href="#" className="fa fa-facebook" title="facebook"></a>
                                    <a href="#" className="fa fa-twitter" title="twitter"></a>
                                    <a href="#" className="fa fa-google-plus" title="google"></a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
);
export default Team;