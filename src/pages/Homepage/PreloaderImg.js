import React from 'react';

const PreloaderImg = (props) => (
    <div className="preloader">
        <div className="preloader_image"></div>
    </div>
);
export default PreloaderImg;