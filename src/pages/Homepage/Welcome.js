import React from "react";
import bg from '../../assets/images/parallax/hello-bg.png';
import square1 from '../../assets/images/square/01.jpg';
import square2 from '../../assets/images/square/05.jpg';
import square3 from '../../assets/images/square/12.jpg';
import square4 from '../../assets/images/square/13.jpg';


const Welcome = (props) => (

    <section className="ls hello s-pt-50 s-pt-sm-150 s-pt-lg-100 s-pt-xl-150 c-mb-30 c-gutter-60">
        <div className="container">
            <div className="row">
                <div className="col-5 d-none d-md-block">
                    <img src={bg} alt="" />
                </div>
                <div className="col-md-7 col-12">
                    <div className="divider-60 d-none d-xl-block"></div>
                    <div className="title-section text-center text-md-left">
                        <span className="sub-title absolute-subtitle">Excellence</span>
                        <h3 className="special-heading"> <span><span>B</span>lowtouch<br /></span>Mobile Salon</h3>
                    </div>
                    <div className="divider-60 d-none d-xl-block"></div>
                    <div className="row">

                        <div className="col-lg-6">
                            <p className="text-center text-md-left">Our talented team of experts breathes in fashion ,  will create a personalized look to suit your individuality, features and hair texture.
                            </p>
                        </div>
                        <div className="col-lg-6">
                            <p className="text-center text-md-left">Blowtouch will help you select a stylist in your location, with the convenience of having your services in your home. </p>
                        </div>
                    </div>
                    <div className="divider-40 d-none d-xl-block"></div>
                    <div className="row c-gutter-30 justify-content-start">
                        <div className="owl-carousel" data-loop="true" data-margin="30" data-nav="false" data-dots="true" data-center="false" data-items="1" data-autoplay="false" data-responsive-xs="1" data-responsive-sm="1" data-responsive-md="2" data-responsive-lg="3">
                            <div className="step-item">
                                <div className="hello-img text-center">
                                    <img src={square2} width="200" height="200" alt="" />
                                    <span>Haircut</span>
                                </div>
                                <div className="hello-content text-center">
                                    <h3></h3>
                                    <p></p>
                                </div>
                            </div>
                            <div className="step-item">
                                <div className="hello-item">
                                    <div className="hello-img text-center">
                                        <img src={square3} width="200" height="200" alt="" />
                                        <span>Makeup</span>
                                    </div>
                                    <div className="hello-content text-center">
                                        <h3></h3>
                                        <p></p>
                                    </div>
                                </div>
                            </div>


                            <div className="step-item">
                                <div className="hello-item">
                                    <div className="hello-img text-center">
                                        <img src={square4} width="200" height="200" alt="" />
                                        <span>Hair Coloring </span>
                                    </div>
                                    <div className="hello-content text-center">
                                        <h3></h3>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                    


                        </div>

                    </div>


                </div>
            </div>
        </div>
    </section>


);
export default Welcome;