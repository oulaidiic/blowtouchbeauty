import React from 'react';
import myVideo from "../../assets/images/Beauty_Fashion.mp4";
import img3 from "../../assets/images/home_3.jpg";

const PageSlider = (props) => (

    <section className="page_slider cover-image cs text-center">
        <img src={img3} alt="img" />
        <div className="flex-bg ds z-6 s-overlay">
            <video muted loop id="myVideo">
                <source src={myVideo} type="video/mp4" />
            </video>
        </div>

        <div className="container-fluid">
            <div className="row">
                <div className="col-md-12">
                    <div className="intro_layers_wrapper">
                        <div className="intro_layers">
                            <div className="intro_layer" data-animation="fadeInRight">
                                <div className="d-inline-block">
                                    <h4 className="text-uppercase intro_before_featured_word">
                                        Blowtouch
                                </h4>
                                </div>
                            </div>
                            <div className="intro_layer" data-animation="fadeInRight">
                                <div className="d-inline-block">
                                    <h2 className="text-uppercase intro-2 intro_featured_word">
                                        Salon
                                </h2>
                                </div>
                            </div>
                            <div className="intro_layer mt-30" data-animation="fadeInUp">
                                <p className="intro_after_featured_word text-center"> We’re the difference between an average fashion style and an exceptional one.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

);
export default PageSlider;