import React from 'react';
import appointmentBg from "../../assets/images/parallax/appoitment-bg.jpg";

const Appointment = (props) => (
    <section className="ls appointment s-pt-60 s-pt-md-0 container-px-xl-165 container-px-lg-40 container-px-md-10">
        <div className="container-fluid">
            <div className="row">
                <div className="col-12 col-md-6 col-xl-6 d-none d-md-block">
                    <img src={appointmentBg} alt="" />
                </div>
                <div className="col-12 col-md-6 col-xl-6">

                    <form className="appointment-form c-mb-20 c-gutter-20" method="post" action="/">
                        <div className="divider-150 d-none d-xl-block"></div>
                        <span className="sub-title text-center text-md-left"></span>
                        <h3 className="special-heading text-center text-md-left">Book Now </h3>
                        <div className="divider-70 d-none d-xl-block"></div>
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="form-group">
                                    <i className="flaticon-profile"></i>
                                    <input type="text" name="name" className="form-control" placeholder="full name" />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <i className="flaticon-clock"></i>
                                    <input type="tel" name="phone" className="form-control" placeholder="time" />
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <i className="flaticon-cut"></i>
                                    <input type="text" name="city" className="form-control" placeholder="service" />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="form-group">
                                    <i className="flaticon-clip"></i>
                                    <textarea rows="5" cols="45" name="message" className="form-control" placeholder="extra notes"></textarea>
                                </div>
                            </div>
                        </div>
                        <div className="row ">
                            <div className="col-sm-12 text-center text-md-left">
                                <div className="divider-60 d-none d-xl-block"></div>
                                <div className="divider-20 d-xl-none"></div>
                                <div className="form-group justify-content-center justify-content-md-start">
                                    <button type="submit" className="btn btn-outline-maincolor">make Appointment</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
);
export default Appointment;