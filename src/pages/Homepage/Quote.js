import React from "react";
import signature from '../../assets/images/signature.png';
import square6 from '../../assets/images/square/06.jpg';
import square7 from '../../assets/images/square/07.jpg';
import square8 from '../../assets/images/square/08.jpg';

const Quote = (props) => (
    <section className="ls quote-section  s-py-60 s-pt-lg-90 s-pb-lg-100 s-pt-xl-160 s-pb-xl-150">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="owl-carousel" data-loop="true" data-margin="30" data-nav="true" data-dots="true" data-center="false" data-items="1" data-autoplay="false" data-responsive-xs="1" data-responsive-sm="1" data-responsive-md="1" data-responsive-lg="1">
                        <div className="quote-item">
                            <div className="quote__image">
                                <img src={square8} alt="" />
                            </div>
                            <blockquote>
                                <footer>
                                    <cite>Jesse D. Caballero, <span>Makeup Client</span></cite>
                                </footer>
                                <p>
                                    Lorem ipsum dolor amet consetetur sadipscing elitr sdiam nonumy eirmod tempor invidunt labore dolore magna aliquyam erat sed diam.
										</p>
                                <img src={signature} alt="" />
                            </blockquote>
                        </div>
                        <div className="quote-item">
                            <div className="quote__image">
                                <img src={square7} alt="" />
                            </div>
                            <blockquote>
                                <footer>
                                    <cite>Jesse D. Caballero, <span>Makeup Client</span></cite>
                                </footer>
                                <p>
                                    Lorem ipsum dolor amet, consetetur sadipscing elitr sdiam nonumy eirmod tempor invidunt labore dolore magna aliquyam erat sed diam.
										</p>
                                <img src={signature} alt="" />
                            </blockquote>
                        </div>
                        <div className="quote-item">
                            <div className="quote__image">
                                <img src={square6} alt="" />
                            </div>
                            <blockquote>
                                <footer>
                                    <cite>Jesse D. Caballero, <span>Makeup Client</span></cite>
                                </footer>
                                <p>
                                    Lorem ipsum dolor amet, consetetur sadipscing elitr sdiam nonumy eirmod tempor invidunt labore dolore magna aliquyam erat sed diam.
										</p>
                                <img src={signature} alt="" />
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

);
export default Quote;