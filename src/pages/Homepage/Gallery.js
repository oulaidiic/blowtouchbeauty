import React from "react";
import gallery1 from '../../assets/images/gallery/01.jpg';
import gallery2 from '../../assets/images/gallery/02.jpg';
import gallery3 from '../../assets/images/gallery/03.jpg';
import gallery4 from '../../assets/images/gallery/04.jpg';
import gallery5 from '../../assets/images/gallery/05.jpg';
import gallery6 from '../../assets/images/gallery/06.jpg';
import gallery7 from '../../assets/images/gallery/07.jpg';
import gallery8 from '../../assets/images/gallery/08.jpg';
import gallery9 from '../../assets/images/gallery/09.jpg';
import gallery10 from '../../assets/images/gallery/10.jpg';
import gallery11 from '../../assets/images/gallery/11.jpg';
import gallery12 from '../../assets/images/gallery/12.jpg';
import gallery13 from '../../assets/images/gallery/13.jpg';
import gallery14 from '../../assets/images/gallery/14.jpg';
import gallery15 from '../../assets/images/gallery/15.jpg';
import gallery16 from '../../assets/images/gallery/16.jpg';
import gallery17 from '../../assets/images/gallery/17.jpg';
import gallery18 from '../../assets/images/gallery/18.jpg';
import gallery19 from '../../assets/images/gallery/19.jpg';

let items = [
    {
    name: gallery1,
    caption: "En voiture",
    description: "Il a pour lui léternité, montre en main.<br /> — Jules Renard"
    },
    {
    name: gallery2,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery3,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery4,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery5,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery6,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery7,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery8,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery9,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery10,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery11,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery12,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery13,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery14,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery15,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery16,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery17,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
    ,
    {
    name: gallery18,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    },
    {
    name: gallery19,
    caption: "Amis",
    description: "Le temps confirme lamitié.<br /> — Henri Lacordaire"
    }
]

const Gallery = (props) => (
    
	<section className="main-gallery2 container-px-0 mt-10">
            <div className="container-fluid">
                <div className="row">
                    <div id="gallery" className="fullscreen" data-items={JSON.stringify(items)}></div>
                    <div id="nav" className="navbar">
                        <button id="preview">&lt; précédent</button>
                        <button id="next">suivant &gt;</button>

                        <a id="copyright" href="http://www.glicer.com">
                            <small>Dev & Rock 'n' roll © Glicer</small>
                        </a>
                    </div>
                </div>
            </div>


    </section>     
);
export default Gallery;