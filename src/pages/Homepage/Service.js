import React from "react";
import service1 from "../../assets/images/services/01.jpg";
import service2 from "../../assets/images/services/02.jpg";
import service3 from "../../assets/images/services/03.jpg";
import service4 from "../../assets/images/services/04.jpg";


const Service = (props) => (
    <section className="ls s-pt-60 s-pt-lg-110 s-pt-xl-145 s-pb-60 s-pb-lg-70 s-pb-xl-110 service">
        <div className="container">
            <div className="row">
                <div className="col-12 col-lg-6 text-center text-lg-left">
                    <span className="sub-title">Featured</span>
                    <h3 className="special-heading">Most Popular Services</h3>
                    <p>Consetetur sadipscing elitr, sed nonumy eirmod tempor invidunt ut labore dolore.</p>
                    <div className="divider-110 d-none d-xl-block"></div>
                    <div className="divider-40 d-xl-none"></div>
                    <div className="sevices-single">
                        <div className="sevices-single__image">
                            <a href="service-single.html"><img src={service1} width="500" height="600" alt="" /></a>
                        </div>
                        <a href="service-single.html">
                            <h4>Professional Makeup</h4>
                        </a>
                        <a href="service-single.html"><span>Makeup</span></a>
                    </div>
                    <div className="sevices-single">
                        <div className="sevices-single__image">
                            <a href="service-single.html"><img src={service2} width="400" height="480" alt="" /></a>
                        </div>
                        <a href="service-single.html">
                            <h4>Tattoo & Piercing</h4>
                        </a>
                        <a href="service-single.html"><span>Modifications</span></a>
                    </div>
                </div>
                <div className="col-12 col-lg-6 text-center text-lg-right">
                    <div className="sevices-single text-left">
                        <div className="sevices-single__image">
                            <a href="service-single.html"><img src={service3} width="400" height="480" alt="" /></a>
                        </div>
                        <a href="service-single.html">
                            <h4>Beard Cutting Experts</h4>
                        </a>
                        <a href="service-single.html"><span>Hair</span></a>
                    </div>
                    <div className="sevices-single text-left">
                        <div className="sevices-single__image">
                            <a href="service-single.html"><img src={service4} width="500" height="600" alt="" /></a>
                        </div>
                        <a href="service-single.html">
                            <h4>Manicure & Pedicure Services</h4>
                        </a>
                        <a href="service-single.html"><span>Nails</span></a>
                    </div>
                    <div className="services-button text-center text-lg-left">
                        <a href="services.html" className="btn btn-outline-maincolor">all services</a>
                    </div>

                </div>
            </div>
        </div>
    </section>
);
export default Service;