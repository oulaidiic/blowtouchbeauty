import React from 'react';
import bg1 from '../../assets/images/parallax/video_background_1.jpg';
import bg2 from '../../assets/images/parallax/video_background_2.jpg';

const Video = (props) => (
    <section className="ls ms s-overlay s-parallax video-section s-pt-295 s-pt-sm-410 s-pt-md-360 s-pb-md-100 s-pt-lg-395 s-pb-lg-150 s-pt-xl-430 s-pb-xl-230 s-pb-60">

        <div className="container">
            <div className="row">
                <div className="col-xl-2">
                    <h3 className="special-heading video-title text-center text-xl-left">Our Video Presentation</h3>
                </div>
                <div className="col-xl-10">
                    <div className="owl-carousel" data-loop="true" data-margin="30" data-nav="true" data-dots="true" data-center="false" data-items="1" data-autoplay="false" data-responsive-xs="1" data-responsive-sm="1" data-responsive-md="1" data-responsive-lg="1">
                        <div className="video-simple">
                            <a href="../../assets/parallax/video_background_1.jpg" className="photoswipe-link" data-width="800" data-height="800" data-iframe="https://www.youtube.com/embed/mcixldqDIEQ">
                                <img src={bg1} alt="img" />
                            </a>
                        </div>
                        <div className="video-simple">
                            <a href="../../assets/parallax/video_background_2.jpg" className="photoswipe-link" data-width="800" data-height="800" data-iframe="https://www.youtube.com/embed/mcixldqDIEQ">
                                <img src={bg2} alt="img" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
);
export default Video;