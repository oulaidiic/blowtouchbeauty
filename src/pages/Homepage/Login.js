import React, { useRef, useState } from 'react'
import socialMediaAuth from '../../backend/services/auth';
import { googleProvider } from '../../backend/services/authMethods';
import { Link, useHistory } from "react-router-dom"
import { useAuth } from "../../contexts/AuthContext"
import {Alert} from "react-bootstrap"

export default function Login() {
    const handleGoogleClick = (e) => {
        e.preventDefault();

        const res = socialMediaAuth(googleProvider);

        console.log(res);
    };
    const emailRef = useRef()
    const passwordRef = useRef()
    const { login } = useAuth()
    const [error, setError] = useState("")
    const [loading, setLoading] = useState(false)
    const history = useHistory()
  
    async function handleSubmit(e) {
      e.preventDefault()
  
      try {
        setError("")
       // setLoading(true)
        await login(emailRef.current.value, passwordRef.current.value)
        
        history.push("/profile")
      } catch {
        setError("Failed to log in")
      }
  
      //setLoading(false)
    }
    return (
        <div>
   <h2 className="text-center mb-4">Log In</h2>
          {error && <Alert variant="danger">{error}</Alert>}
            <form onSubmit={handleSubmit}>

                <div className="form-group has-placeholder">

                    <i className="flaticon-profile"></i>
                    <input type="text" ref={emailRef} className="form-control" placeholder="E-mail" />
                </div>

                <div className="form-group has-placeholder">

                    <i className="grey flaticon-unlock"></i>
                    <input type="password" ref={passwordRef} className="form-control" placeholder="Password" />
                </div>


                <button disabled={loading} type="submit" className="btn btn-outline-maincolor text-center">Log In</button>
                <p></p>

                <button onClick={handleGoogleClick} class="btn google btn-outline-maincolor text-center"><i class="fa fa-google-plus">
                </i> Login using Google
                </button>

                <div className="checkbox">
                    <input type="checkbox" />

                    <a href="#">Forgot password ?</a>
                </div>
            </form>
        </div>
    )
}
