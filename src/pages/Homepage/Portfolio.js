import React from 'react';
import portfolio1 from "../../assets/images/portfolio/01.jpg";
import portfolio2 from "../../assets/images/portfolio/02.jpg";
import portfolio3 from "../../assets/images/portfolio/03.jpg";
import portfolio4 from "../../assets/images/portfolio/04.jpg";
import portfolio5 from "../../assets/images/portfolio/05.jpg";

const Portfolio = (props) => (
    <section className="ls ms portfolio-section s-pt-60 s-pb-60 s-pt-lg-110 s-pb-lg-90  s-pt-xl-150 s-pb-xl-140 s-overlay s-parallax container-px-xl-165 container-px-lg-100 container-px-md-10 c-gutter-70">
        <div className="container-fluid">
            <div className="row">
                <div className="col-xl-5 text-center text-lg-left">
                    <div className="title-portfolio">
                        <span className="sub-title">Check Out</span>
                        <h3 className="special-heading">Our
                        Portfolio</h3>
                        <p>Stet clita kasd gubergren, no takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor. </p>
                    </div>

                </div>
                <div className="col-xl-7">
                    <div className="owl-carousel" data-loop="true" data-margin="30" data-nav="true" data-dots="true" data-center="false" data-items="1" data-autoplay="false" data-responsive-xs="1" data-responsive-sm="1" data-responsive-md="1" data-responsive-lg="1" data-filters=".portfolio-filters">
                        <div className="posrtfolio-single hair other">
                            <img src={portfolio1} alt="" />
                        </div>
                        <div className="posrtfolio-single hair makeup">
                            <img src={portfolio2} alt="" />
                        </div>
                        <div className="posrtfolio-single makeup nails">
                            <img src={portfolio3} alt="" />
                        </div>
                        <div className="posrtfolio-single nails">
                            <img src={portfolio4} alt="" />
                        </div>
                        <div className="posrtfolio-single other">
                            <img src={portfolio5} alt="" />
                        </div>
                    </div>
                    <div className="filters portfolio-filters text-lg-right">
                        <a href="#" data-filter="*" className="active selected">All</a>
                        <a href="#" data-filter=".hair">Hair</a>
                        <a href="#" data-filter=".makeup">Makeup</a>
                        <a href="#" data-filter=".nails">Nails</a>
                        <a href="#" data-filter=".other">Other</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
);
export default Portfolio;