import React from 'react';
import blog2 from '../../assets/images/blog/02.jpg';
import blog10 from '../../assets/images/blog/10.jpg';
import blog11 from '../../assets/images/blog/11.jpg';


const Post = (props) => (

    <section className="ls post-section s-pt-xl-0 s-pb-50 s-pb-lg-100 s-pb-xl-140 container-px-xl-0 container-px-xl-20">
        <div className="container-fluid">
            <div className="row">
                <div className="col-12">
                    <div className="owl-carousel" data-loop="true" data-margin="30" data-nav="true" data-dots="true" data-center="true" data-items="1" data-autoplay="false" data-responsive-xs="1" data-responsive-sm="1" data-responsive-md="3" data-responsive-lg="3">

                        <article className="text-center text-md-left vertical-item post type-post status-publish format-standard">
                            <div className="item-media post-thumbnail">
                                <a href="blog-single-@@type.html">
                                    <img src={blog10} alt="img" />
                                </a>
                            </div>

                            <div className="item-content">
                                <header className="entry-header">
                                    <a href="blog-@@type.html">
                                        Barber
                                        </a>
                                </header>

                                <div className="entry-content">
                                    <h3 className="entry-title">
                                        <a href="blog-single-@@type.html" rel="bookmark">
                                            Lorem ipsum dolor sit amet, consetetur
                                            </a>
                                    </h3>
                                </div>
                            </div>
                        </article>
                        <article className="text-center text-md-left vertical-item post type-post status-publish format-standard">
                            <div className="item-media post-thumbnail">
                                <a href="blog-single-@@type.html">
                                    <img src={blog11} alt="img" />
                                </a>
                            </div>
                            <div className="item-content">
                                <header className="entry-header">
                                    <a href="blog-@@type.html">
                                        Tattoo
                                        </a>
                                </header>

                                <div className="entry-content">
                                    <h3 className="entry-title">
                                        <a href="blog-single-@@type.html" rel="bookmark">
                                            Elitr sed diam nonumy eirmod tempor
                                            </a>
                                    </h3>
                                </div>
                            </div>
                        </article>
                        <article className="text-center text-md-left vertical-item post type-post status-publish format-standard">
                            <div className="item-media post-thumbnail">
                                <a href="blog-single-@@type.html">
                                    <img src={blog2} alt="img" />
                                </a>
                            </div>
                            <div className="item-content">
                                <header className="entry-header">
                                    <a href="blog-@@type.html">
                                        Microblading
                                        </a>
                                </header>

                                <div className="entry-content">
                                    <h3 className="entry-title">
                                        <a href="blog-single-@@type.html" rel="bookmark">
                                            Elitr sed diam nonumy eirmod tempor
                                            </a>
                                    </h3>
                                </div>

                            </div>

                        </article>

                        <article className="text-center text-md-left vertical-item post type-post status-publish format-standard">
                            <div className="item-media post-thumbnail">
                                <a href="blog-single-@@type.html">
                                    <img src={blog2} alt="img" />
                                </a>
                            </div>

                            <div className="item-content">
                                <header className="entry-header">
                                    <a href="blog-@@type.html">
                                        Makeup
                                        </a>
                                </header>

                                <div className="entry-content">
                                    <h3 className="entry-title">
                                        <a href="blog-single-@@type.html" rel="bookmark">
                                            Dolore magna aliquyam erat sed diam voluptua
                                            </a>
                                    </h3>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
);
export default Post;