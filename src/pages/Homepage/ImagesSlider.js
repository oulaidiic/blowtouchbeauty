import React from "react";
import square1 from '../../assets/images/square/01.jpg';
import square2 from '../../assets/images/square/02.jpg';
import square3 from '../../assets/images/square/03.jpg';
import square4 from '../../assets/images/square/04.jpg';
import square5 from '../../assets/images/square/05.jpg';
import square6 from '../../assets/images/square/06.jpg';
import square7 from '../../assets/images/square/07.jpg';
import square8 from '../../assets/images/square/08.jpg';
import square9 from '../../assets/images/square/09.jpg';
import square10 from '../../assets/images/square/10.jpg';
import square11 from '../../assets/images/square/11.jpg';


const ImagesSlider = (props) => (
    <section className="ls c-gutter-0 container-px-0">
        <div className="container-fluid">
            <div className="row">
                <div className="col-sm-12 text-center">

                    <div className="owl-carousel small-gallery-carousel" data-margin="0" data-responsive-lg="8" data-responsive-md="4" data-responsive-sm="3" data-responsive-xs="2" data-nav="true" data-loop="true">
                        <a href="../../assets/images/square/01.jpg" className="photoswipe-link" data-width="800" data-height="800">
                            <img src={square1} alt="" />
                        </a>
                        <a href="../../assets/images/square/02.jpg" className="photoswipe-link" data-width="800" data-height="800">
                            <img src={square2} alt="" />
                        </a>
                        <a href="../../assets/images/square/03.jpg" className="photoswipe-link" data-width="800" data-height="800">
                            <img src={square3} alt="" />
                        </a>
                        <a href="../../assets/images/square/04.jpg" className="photoswipe-link" data-width="800" data-height="800">
                            <img src={square4} alt="" />
                        </a>
                        <a href="../../assets/images/square/05.jpg" className="photoswipe-link" data-width="800" data-height="800" data-iframe="https://www.youtube.com/embed/mcixldqDIEQ">
                            <img src={square5} alt="" />
                        </a>
                        <a href="../../assets/images/square/06.jpg" className="photoswipe-link" data-width="800" data-height="800" data-iframe="//player.vimeo.com/video/1084537">
                            <img src={square6} alt="" />
                        </a>
                        <a href="../../assets/images/square/07.jpg" className="photoswipe-link" data-width="800" data-height="800">
                            <img src={square7} alt="" />
                        </a>
                        <a href="../../assets/images/square/08.jpg" className="photoswipe-link" data-width="800" data-height="800">
                            <img src={square8} alt="" />
                        </a>
                        <a href="../../assets/images/square/09.jpg" className="photoswipe-link" data-width="800" data-height="800">
                            <img src={square9} alt="" />
                        </a>
                        <a href="../../assets/images/square/10.jpg" className="photoswipe-link" data-width="800" data-height="800">
                            <img src={square10} alt="" />
                        </a>
                        <a href="../../assets/images/square/11.jpg" className="photoswipe-link" data-width="800" data-height="800">
                            <img src={square11} alt="" />
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </section>
);
export default ImagesSlider;