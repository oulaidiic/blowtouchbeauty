import React from "react";

const TopLine = (props) => (
    <section className="page_topline ds ms s-py-3 c-my-15 container-px-40">
        <div className="container-fluid">
            <div className="row align-items-center flex-column flex-md-row">
                <div className="col-xl-4 col-md-6 col-12 text-center text-md-left">
                    <ul className="top-includes contact">
                        <li>
                            <span className="icon-inline">
                                <span className="icon-styled">
                                    <i className="flaticon-headphones"></i>
                                </span>
                                <span>
                                    1-800-123-4567
										</span>
                            </span>
                        </li>
                        <li>
                            <span className="icon-inline">
                                <span className="icon-styled">
                                    <i className="flaticon-envelope"></i>
                                </span>
                                <span>
                                    info@blowtouch.com
										</span>
                            </span>
                        </li>

                    </ul>
                </div>

                <div className="col-xl-4 col-md-6 col-12 text-center text-xl-center">
                    <div className="social-top-includ">
                        <p className="social-icons">
                            <a href="#" className="fa fa-facebook" title="facebook"></a>
                            <a href="#" className="fa fa-twitter" title="twitter"></a>
                            <a href="#" className="fa fa-google" title="google"></a>
                            <a href="#" className="fa fa-instagram" title="google"></a>
                            <a href="#" className="fa fa-youtube-play" title="google"></a>
                        </p>
                    </div>

                    <div className="top-includ top-2 justify-content-center justify-content-xl-end">

                        <div className="dropdown">
                            <a className="dropdown-toggle dropdown-shopping-cart" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i className="flaticon-bag"></i>
                                <span className="badge bg-maincolor">2</span>
                            </a>
                            <div className="dropdown-menu dropdown-menu-right ls">
                                <div className="widget woocommerce widget_shopping_cart">


                                    <div className="widget_shopping_cart_content">

                                        <ul className="woocommerce-mini-cart cart_list product_list_widget ">
                                            <li className="woocommerce-mini-cart-item mini_cart_item">
                                                <a href="#" className="remove" aria-label="Remove this item" data-product_id="73" data-product_sku="">×</a>
                                                <a href="shop-product-right.html">
                                                    <img src="images/shop/26.jpg" alt="" />Ship Your Idea
														</a>

                                                <span className="quantity">1 ×
															<span className="woocommerce-Price-amount amount">
                                                        <span className="woocommerce-Price-currencySymbol">$</span>
                                                        12.00
															</span>
                                                </span>
                                            </li>
                                            <li className="woocommerce-mini-cart-item mini_cart_item">
                                                <a href="#" className="remove" aria-label="Remove this item" data-product_id="76" data-product_sku="">×</a>
                                                <a href="shop-product-right.html">
                                                    <img src="images/shop/26.jpg" alt="" />Woo Album #1
														</a>

                                                <span className="quantity">1 ×
															<span className="woocommerce-Price-amount amount">
                                                        <span className="woocommerce-Price-currencySymbol">$</span>
                                                        15.00
															</span>
                                                </span>
                                            </li>
                                        </ul>

                                        <p className="woocommerce-mini-cart__total total">
                                            <strong>Subtotal:</strong>
                                            <span className="woocommerce-Price-amount amount">
                                                <span className="woocommerce-Price-currencySymbol">$</span>
                                                27.00
													</span>
                                        </p>

                                        <p className="woocommerce-mini-cart__buttons buttons">
                                            <a href="shop-cart.html" className="button wc-forward">View cart</a>
                                            <a href="shop-cart.html" className="button wc-forward checkout">checkout</a>
                                        </p>

                                    </div>
                                </div>

                            </div>
                        </div>


                        {/* <!--login form --> */}
                        <div className="dropdown">
                            <a className="" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i className="flaticon-profile"></i>
                            </a>

                            <div className="dropdown-menu dropdown-menu-right text-center">

                                <div className="dropdown-top">
                                    <a href="">log in</a>
                                    <a href="">register</a>
                                </div>

                                <form>

                                    <div className="form-group has-placeholder">

                                        <i className="flaticon-profile"></i>
                                        <input type="text" className="form-control" placeholder="login" />
                                    </div>

                                    <div className="form-group has-placeholder">

                                        <i className="grey flaticon-unlock"></i>
                                        <input type="password" className="form-control" placeholder="Password" />
                                    </div>

                                    <div className="checkbox">
                                        <input type="checkbox" />

                                        <a href="#">Forgot password?</a>
                                    </div>
                                    <div className="">

                                    </div>

                                    <button type="submit" className="btn btn-outline-maincolor text-center">Log In</button>

                                </form>

                            </div>
                            {/* <!--.dropdown menu--> */}
                        </div>
                        {/* <!--modal search--> */}
                        <span>
                            <a href="#" className="search_modal_button">
                                <i className="flaticon-search"></i>
                            </a>
                        </span>

                    </div>

                </div>
                {/* <div className="col-xl-4 text-md-right d-none d-xl-block text-center">

                    <div className="top-includes">
                        <span><span className="icon-styled"><i className="flaticon-timer"></i></span><strong>Mon-Fri: </strong>8:00-18:00; <strong>Sat: </strong>9:00-17:00; <strong>Sun: </strong>off</span>
                    </div>

                </div> */}
            </div>
        </div>
    </section>


);
export default TopLine;