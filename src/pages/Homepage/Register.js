import React, { useRef, useState } from 'react'
import socialMediaAuth from '../../backend/services/auth';
import { googleProvider } from '../../backend/services/authMethods';
import { Link, useHistory } from "react-router-dom"
import { useAuth } from "../../contexts/AuthContext"
export default function Register() {
    const handleGoogleClick = (e) => {
        e.preventDefault();

        const res = socialMediaAuth(googleProvider);

        console.log(res);
    };
    const emailRef = useRef()
    const passwordRef = useRef()
    const passwordConfirmRef = useRef()
    const { signup } = useAuth()
    const [error, setError] = useState("")
    const [loading, setLoading] = useState(false)
    const history = useHistory()

    async function handleSubmit(e) {
        e.preventDefault()
        console.log(emailRef.current.value);
        if (passwordRef.current.value !== passwordConfirmRef.current.value) {
            return setError("Passwords do not match")
        }

        try {
            setError("")
            setLoading(true)
            await signup(emailRef.current.value, passwordRef.current.value)
            history.push("/profile")
        } catch {
            setError("Failed to create an account")
        }

        setLoading(false)
    }
    return (
        <div>
            <form onSubmit={handleSubmit}>

                <div className="form-group has-placeholder">

                    <i className="flaticon-profile"></i>
                    <input id="email" type="text" ref={emailRef} className="form-control" placeholder="login" />
                </div>

                <div className="form-group has-placeholder">

                    <i className="grey flaticon-unlock"></i>
                    <input id="password" type="password" ref={passwordRef} className="form-control" placeholder="Password" />
                </div>
                <div className="form-group has-placeholder">

                    <i className="grey flaticon-unlock"></i>
                    <input id="password" type="password" ref={passwordConfirmRef} className="form-control" placeholder="Confirm Password" />
                </div>
                <p>

                </p>

                <button type="submit" disabled={loading} className="btn btn-outline-maincolor text-center">Register</button>
                {/* 
                <div className="checkbox">
                    <input type="checkbox" />

                    <a href="#">Already have an account? Log In</a>
                </div> */}

            </form>
        </div>
    )
}