import React from "react";
import Footer from "../../components/Footer";
import PreloaderImg from "./PreloaderImg";
import Modal from "./Modal";
import TopLine from "./TopLine";
import Header from "../../components/Header";
import PageSlider from "./PageSlider";
import Gallery from "./Gallery";
import Welcome from "./Welcome";
import Video from "./Video";
import Appointment from "./Appointment";
import Service from "./Service";
import Portfolio from "./Portfolio";
import Quote from "./Quote";
import Post from "./Post";
import Team from "./Team";
import ImagesSlider from "./ImagesSlider";

const Homepage = (props) => (
    <>
        <PreloaderImg />
        <Modal />
        <div id="canvas">
            <div id="box_wrapper">
                <TopLine />
                <Header />
                <PageSlider />
                {/* <Gallery /> */}
                <Welcome />
                {/* <Video /> */}
                <Appointment />
                {/* <Service />
                <Portfolio />
                <Quote />
                <Post /> */}
                <Team />
                <ImagesSlider />
            </div>
        </div>
        <Footer />
    </>
);
export default Homepage;