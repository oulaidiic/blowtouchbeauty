// Import the functions you need from the SDKs you need
import firebase from 'firebase';
// import  'firebase/auth';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyABW1I-3Qm7sfo1lTANi6W94kUM0MbP_F4",
  authDomain: "blowtouch-d916d.firebaseapp.com",
  projectId: "blowtouch-d916d",
  storageBucket: "blowtouch-d916d.appspot.com",
  messagingSenderId: "623983732026",
  appId: "1:623983732026:web:37b10fe9c43e1758a41c03",
  measurementId: "G-YK03W0GZHN"
};

// Initialize Firebase
const init  = firebase.initializeApp(firebaseConfig);
console.log("firebase init")
export const auth = init.auth()
export default firebase;