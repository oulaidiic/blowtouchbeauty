import firebase from './config/firebase-config';
 const socialMediaAuth = (provider) => {
return firebase
.auth()
.signInWithPopup(provider)
 .then((result) => {
   
    const user = result.user;

   return user;
   // ...
 }).catch((error) => {
   // Handle Errors here.
   const errorCode = error.code;
   const errorMessage = error.message;
   // The email of the user's account used.
   const email = error.email;
   // The AuthCredential type that was used.
//    const credential = GoogleAuthProvider.credentialFromError(error);
return error;
   // ...
 });};
export default socialMediaAuth;