import Homepage from "./pages/Homepage/Homepage";
import {AuthProvider} from "../src/contexts/AuthContext"
import ProfilePage from "./pages/Profile/ProfilePage";
import PrivateRoute from "./PrivateRoute";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

function App() {
  return (
    <Router>
    <AuthProvider>
    <Switch>
    <Route exact path="/" component={Homepage} />
    <PrivateRoute  path="/profile" component={ProfilePage} />
    </Switch>
    </AuthProvider> 
    </Router>
  );
}

export default App;
